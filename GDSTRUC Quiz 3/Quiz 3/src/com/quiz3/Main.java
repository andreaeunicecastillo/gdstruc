package com.quiz3;

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        ArrayQueue queue = new ArrayQueue(14);

        for(int game = 1; game != 11; ++game) {
            System.out.println("Game: " + game);

            while(queue.size() <= 4) {
                int maxQueue = 7;
                int minQueue = 1;
                int rangeQueue = maxQueue - minQueue + 1;
                int randQueue = (int)(Math.random() * (double)rangeQueue) + 1;
                if (randQueue == 1) {
                    queue.add(new Player(1, "aceu", 188));
                } else if (randQueue == 2) {
                    queue.add(new Player(2, "sinatraa", 100));
                    queue.add(new Player(3, "subroza", 195));
                } else if (randQueue == 3) {
                    queue.add(new Player(4, "ploo", 130));
                    queue.add(new Player(5, "ming", 200));
                    queue.add(new Player(6, "hanzel", 450));
                } else if (randQueue == 4) {
                    queue.add(new Player(7, "len", 13));
                    queue.add(new Player(8, "qey", 20));
                    queue.add(new Player(9, "poun", 43));
                    queue.add(new Player(10, "gret", 90));
                } else if (randQueue == 5) {
                    queue.add(new Player(11, "aceu", 188));
                    queue.add(new Player(12, "sinatraa", 100));
                    queue.add(new Player(13, "subroza", 195));
                    queue.add(new Player(14, "ploo", 130));
                    queue.add(new Player(15, "ming", 200));
                } else if (randQueue == 6) {
                    queue.add(new Player(16, "aceu", 188));
                    queue.add(new Player(17, "sinatraa", 100));
                    queue.add(new Player(18, "subroza", 195));
                    queue.add(new Player(19, "ploo", 130));
                    queue.add(new Player(20, "ming", 200));
                    queue.add(new Player(21, "hanzel", 450));
                } else if (randQueue == 7) {
                    queue.add(new Player(22, "aceu", 188));
                    queue.add(new Player(23, "sinatraa", 100));
                    queue.add(new Player(24, "subroza", 195));
                    queue.add(new Player(25, "ploo", 130));
                    queue.add(new Player(26, "ming", 200));
                    queue.add(new Player(27, "hanzel", 450));
                    queue.add(new Player(28, "gret", 90));
                }

                System.out.println("Queue Players: " + queue.size());
                queue.printQueue();
            }

            queue.remove();
            queue.remove();
            queue.remove();
            queue.remove();
            queue.remove();
            System.out.println("After removing 5 Queue Players:");
            queue.printQueue();
        }

    }
}
