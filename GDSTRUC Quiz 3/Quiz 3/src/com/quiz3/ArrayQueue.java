package com.quiz3;

import java.util.NoSuchElementException;

public class ArrayQueue {
    private Player[] queue;
    private int front;
    private int back;

    public ArrayQueue(int capacity) {
        this.queue = new Player[capacity];
    }

    public void add(Player player) {
        if (this.back == this.queue.length) {
            Player[] newArray = new Player[this.queue.length * 2];
            System.arraycopy(this.queue, 0, newArray, 0, this.queue.length);
            this.queue = newArray;
        }

        this.queue[this.back] = player;
        ++this.back;
    }

    public Player remove() {
        if (this.size() == 0) {
            throw new NoSuchElementException();
        } else {
            Player removePlayer = this.queue[this.front];
            this.queue[this.front] = null;
            ++this.front;
            if (this.size() == 0) {
                this.front = 0;
                this.back = 0;
            }

            return removePlayer;
        }
    }

    public Player peek() {
        if (this.size() == 0) {
            throw new NoSuchElementException();
        } else {
            return this.queue[this.front];
        }
    }

    public int size() {
        return this.back - this.front;
    }

    public void printQueue() {
        for(int i = this.front; i < this.back; ++i) {
            System.out.println(this.queue[i]);
        }

    }
}
