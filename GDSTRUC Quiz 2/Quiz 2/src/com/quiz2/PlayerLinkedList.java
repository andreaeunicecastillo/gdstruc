package com.quiz2;

public class PlayerLinkedList {
    private PlayerNode head;
    private PlayerLinkedList playerLinkedList;

    public void addToFront(Player player) {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

    public void printList() {
        PlayerNode current = head;
        System.out.print("Head -> ");
        while (current != null) {
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }

    public void removeFirst() {
        head = head.getNextPlayer();
    }

    public void sizeOfElement() {
        PlayerNode current = head;
        int count = 0;
        while (current != null) {
            count++;
            current = current.getNextPlayer();
        }
        System.out.println("No. of elements: " + count);
    }

    public boolean search(Player player) {
        System.out.print("Is there a " + player + "? ");
        PlayerNode current = head;
        while (current != null) {
            if (current.getPlayer() == player)
                return true;
            current = current.getNextPlayer();
        }
        return false;
    }

    public void index(Player player)
    {
        PlayerNode current = head;
        int count = 0;
        while (current.getPlayer() != player)
        {
            count++;
            current = current.getNextPlayer();
        }
        System.out.println("Index of " + player + " is " + count);
    }
}
