package com.quiz2;

import java.awt.*;

public class Main {

    public static void main(String[] args) {

        Player asuna = new Player(1, "Asuna", 100);
        Player lethalBacon = new Player(2, "LethalBacon", 205);
        Player hpDeskjet = new Player(3, "HPDeskjet", 341);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        playerLinkedList.addToFront(asuna);
        playerLinkedList.addToFront(lethalBacon);
        playerLinkedList.addToFront(hpDeskjet);
        playerLinkedList.printList();
        playerLinkedList.sizeOfElement();
        playerLinkedList.removeFirst();
        System.out.println("Remove Function: ");
        playerLinkedList.printList();
        playerLinkedList.sizeOfElement();
        if (playerLinkedList.search(hpDeskjet))
            System.out.println("True");
        else
            System.out.println("False");

        if (playerLinkedList.search(lethalBacon))
            System.out.println("True");
        else
            System.out.println("False");
        playerLinkedList.index(asuna);
        playerLinkedList.index(lethalBacon);
        playerLinkedList.removeFirst();
        System.out.println("Remove Function: ");
        playerLinkedList.printList();
        playerLinkedList.sizeOfElement();









    }
}
