package com.midterms;

import java.util.Stack;

public class Main {

    public static void main(String[] args) {

        Stack<String> cards = new Stack<>();
        Stack<String> pile = new Stack<>();
        Stack<String> hands = new Stack<>();

        cards.push("1 - Two of Spade");
        cards.push("2 - Two of Hearts");
        cards.push("3 - Five of Clubs");
        cards.push("4 - Seven of Spades");
        cards.push("5 - Ten of Diamonds");
        cards.push("6 - Two of Hearts");
        cards.push("7 - Five of Clubs");
        cards.push("8 - Ten of Spades");
        cards.push("9 - Three of Hearts");
        cards.push("10 - Two of Spade");
        cards.push("11 - Six of Spades");
        cards.push("12 - Two of Hearts");
        cards.push("13 - Nine of Diamonds");
        cards.push("14 - Four of Spades");
        cards.push("15 - Six of Hearts");
        cards.push("16 - Ace of Clubs");
        cards.push("17 - Two of Hearts");
        cards.push("18 -Four of Spades");
        cards.push("19 - Ten of Hearts");
        cards.push("20 -Nine of Diamonds");
        cards.push("21 - Two of Spades");
        cards.push("22 - Seven of Diamonds");
        cards.push("23 - Two of Hearts");
        cards.push("24 - Six of Clubs");
        cards.push("25 - Two of Diamonds");
        cards.push("26 - Eight of Hearts");
        cards.push("27 - Four of Clubs");
        cards.push("28 - Five of Diamonds");
        cards.push("29 - Ace of Hearts");
        cards.push("30 - Two of Spades");

        while (cards.size() != 0)
        {
            int maxCommand = 3;
            int minCommand = 1;
            int rangeCommand = maxCommand - minCommand + 1;
            int randCommand = (int) (Math.random() * rangeCommand) + 1;

            int maxCard = 5;
            int minCard = 1;
            int rangeCard = maxCard - minCard + 1;
            int randCard = (int) (Math.random() * rangeCard) + 1;

            if (randCommand == 1)
            {
                System.out.println("Draw " + randCard + " card/s");
                if (cards.size() < randCard)
                {
                    for (int i = 1; i <= cards.size(); i++)
                    {
                        System.out.println(hands.push(cards.pop()));
                    }
                    System.out.println("Not enough card");
                }
                else
                {
                    for (int i = 1; i <= randCard; i++)
                    {
                        System.out.println(hands.push(cards.pop()));
                    }
                }
            }
            else if (randCommand == 2)
            {
                System.out.println("Discard " + randCard + " card/s");
                if (cards.size() < randCard)
                {
                    for (int i = 1; i <= cards.size(); i++)
                    {
                        System.out.println(pile.push(cards.pop()));
                    }
                    System.out.println("Not enough card");
                }
                else
                {
                    for (int i = 1; i <= randCard; i++)
                    {
                        System.out.println(pile.push(cards.pop()));
                    }
                }
            }
            else if (randCommand == 3)
            {
                System.out.println("Get " + randCard + " card/s from the discarded pile");
                if (pile.size() < randCard)
                {
                    for (int i = 1; i <= pile.size(); i++)
                    {
                        System.out.println(hands.push(pile.pop()));
                    }
                    System.out.println("Not enough card");
                }
                else
                {
                    for (int i = 1; i <= randCard; i++)
                    {
                        System.out.println(hands.push(pile.pop()));
                    }
                }
            }
            System.out.println("-------------");
            System.out.println("No. of cards: " + hands.size());
            System.out.println("No. of remaining cards: " + cards.size());
            System.out.println("No. of cards in discarded pile: " + pile.size());
            System.out.println();
        }
        System.out.println("No more remaining cards on the deck. Game Over!");
    }
}



